��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  R   
  5   ]  O   �  C   �  ]   '  W   �  C   �  P   !  F   r  @   �  %   �      	     0	  F   C	  #   �	     �	  "   �	  -   �	  ;   
  "   W
  "   z
     �
  -   �
            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-08 16:10+0300
Last-Translator: Panwar108 <caspian7pena@gmail.com>
Language-Team: Hindi (http://www.transifex.com/anticapitalista/antix-development/language/hi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
     --all		सभी (संक्षिप्त) सूचना देखें     --help		यह सहायता देखें     -a|--audio		ध्वनि कार्ड सूचना देखें     -c|--cpu    	सीपीयू सूचना देखें     -g|--graphics	ग्राफ़िक्स कार्ड सूचना देखें     -hd|--hard-drive	हार्ड ड्राइव सूचना देखें     -l|--system		सिस्टम सूचना देखें     -n|--nic		एनआईसी कार्ड सूचना देखें     -r|--repository	पैकेज-संग्रह देखें     -s|--sensor		सेंसर सूचना देखें सभी संक्षिप्त ऑडियो सीपीयू पैकेज-संग्रह संपादित करें? संपादित करें? ग्राफ़िक्स हार्ड ड्राइव एनआईसी व आईपी पता कंप्यूटर सूचना विकल्प पैकेज-संग्रह सेंसर आउटपुट सिस्टम अपरिचित विकल्प : $1 