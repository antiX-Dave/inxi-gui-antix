��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  (  g  1   �  $   �  4   �  ,     7   I  7   �  .   �  (   �  )     -   ;     i     z          �     �     �     �     �     �     �     �     	     
	            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-27 01:56+0300
PO-Revision-Date: 2018-09-23 22:16+0300
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: Czech (http://www.transifex.com/anticapitalista/antix-development/language/cs/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: cs
Plural-Forms: nplurals=4; plural=(n == 1 && n % 1 == 0) ? 0 : (n >= 2 && n <= 4 && n % 1 == 0) ? 1: (n % 1 != 0 ) ? 2 : 3;
X-Generator: Poedit 1.8.11
     --all		zobrazit všechny (krátké) informace     --help		zobrazit tuto nápovědu     -a|--audio		zobrazit informace o zvukové kartě     -c|--cpu 	zobrazit informace o procesoru     -g|--graphics	zobrazit informace o grafické kartě     -hd|--hard-drive	zobrazit informace o pevném disku     -l|--system		zobrazit informace o systému     -n|--nic 	 	zobrazit informace o nic     -r|--repository	zobrazit úložiště     -s|--sensor		zobrazit informace o senzoru Všechno krátce Zvuk Procesor Upravit úložiště? Upravit? Grafika Pevný disk NIC a IP adresa Možnosti informací o PC Úložiště Výstup senzoru Systém Nerozpoznaná možnost: $1 