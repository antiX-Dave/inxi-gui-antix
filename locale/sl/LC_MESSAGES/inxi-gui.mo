��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  ,   E     r  6   �  3   �  :   �  1   6  /   h  &   �  (   �  .   �          %     *     3  	   H     R  	   Z     d  '   u     �     �     �     �            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-03-21 13:25+0200
Last-Translator: Arnold Marko <arnold.marko@gmail.com>
Language-Team: Slovenian (http://www.transifex.com/anticapitalista/antix-development/language/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 2.3
     --all		prikaži vse (kratke) informacije     --help	prikaži to pomoč     -a|--audio		prikaži informacije o zvočni kartici     -c|--cpu    	prikaži informacije od procesorju     -g|--graphics	prikaži informacije o grafični kartici     -hd|--hard-drive	prikaži informacije o disku     -l|--system		prikaži informacije o sistemu     -n|--nic		prikaži nic informacije     -r|--repository	prikaži skladišče     -s|--sensor		prikaži informacije senzorja Vse na kratko Zvok Procesor Urejanje skladišč? Urejanje? Grafika Trdi disk NIC in IP naslov Možnosti za informacije o računalniku Skladišče Izhod senzorja Sistem Neznana opcija: $1 