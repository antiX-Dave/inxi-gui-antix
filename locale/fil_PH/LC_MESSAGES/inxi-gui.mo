��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  .   b  %   �  5   �  /   �  :     :   X  3   �  ,   �  *   �  2     	   R     \     b     f     �     �  
   �     �  $   �  
   �     �     �     �            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-03-21 13:24+0200
Last-Translator: sonny nunag <sonnynunag@yahoo.com>
Language-Team: Filipino (Philippines) (http://www.transifex.com/anticapitalista/antix-development/language/fil_PH/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fil_PH
Plural-Forms: nplurals=2; plural=(n == 1 || n==2 || n==3) || (n % 10 != 4 || n % 10 != 6 || n % 10 != 9);
X-Generator: Poedit 2.3
     --all		tingnan lahat (buod) na impormasyon     --help		ipakita ang tulong na ito     -a|--audio		tingnan ang impormasyon ng sound card     -c|--cpu    	tingnan ang impormasyon ng CPU     -g|--graphics	tingnan ang impormasyon ng graphics card     -hd|--hard-drive	tingnan ang impormasyon ng hard drive     -l|--system		tingnan ang impormasyon ng sistema     -n|--nic		tingnan ang impormasyon ng nic     -r|--repository	tingnan ang repository     -s|--sensor		tingnan ang impormasyon ng sensor All Short Audio CPU Baguhin ang Repositories? Baguhin? Graphics Hard Drive NIC at address ng IP Mga pagpipilian sa impormasyon ng PC Repository Sensor Output System Pagpipilian di kilala: $1 