��          �      |      �  '   �       +   7  %   c  0   �  0   �  (   �  "     #   7  (   [     �     �     �     �     �  
   �     �     �  
   �     �     �  �    P   �  H   �  3   F  .   z  K   �  K   �  4   A  *   v  %   �  0   �     �               *     ;     N     m  '   �  
   �     �  @   �                                          
                                   	                        --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-16 15:38+0300
Last-Translator: VI AhrivenX <pongpeera072@gmail.com>
Language-Team: Thai (http://www.transifex.com/anticapitalista/antix-development/language/th/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: th
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.3
     --all		ดูข้อมูลทั้งหมด (แบบสั้น)     --help		แสดงหน้าช่วยเหลือนี้     -a|--audio		ดูข้อมูล Sound card     -c|--cpu    	 ดูข้อมูล CPU     -g|--graphics	ดูข้อมูลกราฟิกการ์ด     -hd|--hard-drive	ดูข้อมูลฮาร์ดไดรฟ์     -l|--system	ดูข้อมูลระบบ     -n|--nic		ดูข้อมูล NIC     -r|--repository	ดู Repository     -s|--sensor		ดูข้อมูล Sensor เสียง CPU แก้ไข Repositories? แก้ไข? กราฟิก ฮาร์ดไดรฟ์ NIC และ IP Address ตัวเลือก PC Information Repository ระบบ ตัวเลือกที่ไม่รู้จัก: $1 