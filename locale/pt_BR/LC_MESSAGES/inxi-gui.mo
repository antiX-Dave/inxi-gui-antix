��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  4   !     V  1   o  )   �  J   �  :     0   Q  =   �  '   �  /   �          '     -     1     G     O     X     f     y     �     �     �     �            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-08 16:09+0300
Last-Translator: marcelo cripe <marcelocripe@gmail.com>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 --all		visualizar todas as informações (resumidas) --help		exibe esta ajuda -a|--audio		visualizar informações da placa de  -c|--cpu 	visualizar informações da CPU -g|--graphics	visualizar informações da placa gráfica (placa de vídeo) -hd|--hard-drive	visualizar informações do disco rígido -l|--system		visualizar informações de sistema -n|--nic		visualizar informações de NIC (interface de rede) -r|--repository	visualizar repositório -s|--sensor		visualizar informações do sensor Tudo abreviado Audio CPU Editar repositórios? Editar? Gráfcos Disco rígido NIC e endereço IP Opções de Informação do PC Repositório Valores dos sensores Sistema Opção não reconhecida: $1 