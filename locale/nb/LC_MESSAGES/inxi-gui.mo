��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  %     #   E  *   i  '   �  /   �  0   �  &     .   D  "   s  *   �     �     �     �     �     �     �           	  "   %  
   H     S     `     g            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-08 16:09+0300
Last-Translator: Kjell Cato Heskjestad <cato@heskjestad.xyz>
Language-Team: Norwegian Bokmål (http://www.transifex.com/anticapitalista/antix-development/language/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
     --all		vis all (kort) informasjon     --help		vis denne hjelpeteksten     -a|--audio		vis informasjon om lydkort     -c|--cpu    	vis informasjon om CPU     -g|--graphics	vis informasjon om grafikkort     -hd|--hard-drive	vis informasjon om harddisk     -l|--system		vis systeminformasjon     -n|--nic		vis informasjon om nettverkskort     -r|--repository	vis pakkearkiv     -s|--sensor		vis informasjon om sensor Kort oppsummering Lyd CPU Rediger pakkearkiv? Rediger? Grafikk Harddisk Nettverkskort og IP-adresse Alternativer for informasjon om PC Pakkearkiv Sensorutdata System Ukjent alternativ: $1 