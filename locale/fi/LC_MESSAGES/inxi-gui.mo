��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  (        :  .   Z  .   �  4   �  4   �  2   "     U  &   s  0   �     �     �     �     �       	     
        &     7     K     Z     k     y            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-03-21 13:24+0200
Last-Translator: Oi Suomi On! <oisuomion@protonmail.com>
Language-Team: Finnish (http://www.transifex.com/anticapitalista/antix-development/language/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 --all 		 katso kaikki tiedot (lyhyesti)      --help		näytä tämä ohje     -a|--audio		näytä tietoja äänikortista     -c|--cpu    	näytä tietoja suorittimesta     -g|--graphics	näytä tietoja näytönohjaimesta     -hd|--hard-drive	näytä tietoja kiintolevyistä     -l|--system		näytä tietoja järjestelmästä  -n|--nic 		 katso nic tiedot -r|--repository 	 katso ohjelmavarasto     -s|--sensor		näytä tietoja laiteantureista Kaikki lyhyttiedot Ääni Suoritin Muokkaa pakettivarastoja? Muokkaa? Grafiikka Kiintolevy NIC ja IP-osoite PC-tietovaihtoehdot Pakettivarasto Anturien tuloste Järjestelmä Tunnistamaton valitsin: $1 