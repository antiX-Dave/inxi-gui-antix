��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  -     !   A  .   c  ,   �  1   �  7   �  *   )  <   T  1   �  1   �     �       	             )  	   :     D     S     d     �     �     �     �            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-03-21 13:26+0200
Last-Translator: mahmut özcan <mahmutozcan@protonmail.com>
Language-Team: Turkish (http://www.transifex.com/anticapitalista/antix-development/language/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 --all		bütün (kısa) bilgiyi görüntüleme --help		bu yardımı görüntüle --al--audio		ses kartı bilgisi görüntüleme -cl--cpu    	işlemci bilgisi görüntüleme -gl--graphics	ekran kartı bilgisi görüntüleme -hdl--hard-drive	sabit sürücü bilgisi görüntüleme -ll--system		sistem bilgisi görüntüleme -nl--nic		ağ arabirimi denetleyicisi bilgisi görüntüleme -rl--repository	yazılım deposunu görüntüleme -sl--sensor		algılayıcı bilgisi görüntüleme Tümü Kısa Ses İşlemci Depolar Düzenlensin mi? Düzenlensin mi? Grafikler Disk Sürücü NIC ve IP adresi Bilgisayar bilgisi seçenekleri Depo Algılayıcı Çıkışı Sistem Tanınmayan seçenek: $1 