��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  .   &     U  .   s  !   �  6   �  6   �  ,   2  %   _  #   �  +   �     �     �     �     �       	             #     5     T     `     u     }            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-11-28 20:44+0200
Last-Translator: David Rebolo Magariños <drgaga345@gmail.com>
Language-Team: Galician (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/gl_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: gl_ES
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
     --all		ver toda a información (abreviada)     --help		amosar esta axuda -a|--audio		ver información da tarxeta do son -c|--cpu 	ver información da CPU     -g|--graphics	ver información da tarxeta gráfica     -hd|--hard-drive	ver información do disco ríxido     -l|--system		ver información do sistema     -n|--nic		ver información do nic     -r|--repository	ver repositorio     -s|--sensor		ver información do sensor Todo abreviado Audio CPU Editar repositorios? Editar? Gráficos Disco ríxido NIC e enderezo IP Opcións de información do PC Repositorio Valores dos sensores Sistema Opción non recoñecida: $1 