��          �   %   �      0  '   1     Y  +   w  %   �  0   �  0   �  (   +  "   T  #   w  (   �  	   �     �     �     �     �     �  
   �            
   /     :     H     O  �  g  %   "     H  &   e  $   �  +   �  ,   �  &   
  !   1  %   S  &   y  
   �     �     �     �     �     �     �     �  !   �          '     5     <            
      	                                                                                                                 --all		view all (short) information     --help		display this help     -a|--audio		view sound card information     -c|--cpu    	view cpu information     -g|--graphics	view graphics card information     -hd|--hard-drive	view hard drive information     -l|--system		view system information     -n|--nic		view nic information     -r|--repository	view repository     -s|--sensor		view sensor information All Short Audio CPU Edit Repositories? Edit? Graphics Hard Drive NIC and IP address PC Information Options Repository Sensor Output System Unrecognized option: $1 Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-10-27 01:56+0300
PO-Revision-Date: 2019-01-25 17:33+0200
Last-Translator: scootergrisen
Language-Team: Danish (http://www.transifex.com/anticapitalista/antix-development/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
     --all		vis al (korte) information     --help		vis denne hjælp     -a|--audio		vis lydkortinformation     -c|--cpu    	vis cpu-information     -g|--graphics	vis grafikkortinformation     -hd|--hard-drive	vis harddiskinformation     -l|--system		vis systeminformation     -n|--nic		vis nic-information     -r|--repository	vis softwarekilde     -s|--sensor		vis sensorinformation Alle korte Lyd CPU Rediger softwarekilder? Rediger? Grafik Harddisk NIC- og IP-adresse Valgmuligheder for PC-information Softwarekilde Sensor-output System Ugenkendt tilvalg: $1 